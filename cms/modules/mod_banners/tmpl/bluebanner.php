<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_banners
 *
 * @copyright   Copyright (C) 2005 - 2020 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JLoader::register('BannerHelper', JPATH_ROOT . '/components/com_banners/helpers/banner.php');

?>
<div class="row">
    <div class="col-12 p-0">
        <div class="banner-img-bg" style="background-image: url(<?php echo $list[0]->params->get('imageurl'); ?>);">
        </div>
    </div>
</div>