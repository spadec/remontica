<?php
/**
 * @package		Joomla.Site
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

?>
<?php
//get template params
$templateparams	=  JFactory::getApplication()->getTemplate(true)->params;

//get language and direction
$doc = JFactory::getDocument();
$this->language = $doc->language;
$this->direction = $doc->direction;
$errorCode = $this->error->getCode();
if($errorCode=="404"){
	header('Location: index.php?option=com_content&view=article&id=45');
	exit;
}
else{
	header('Location: index.php?option=com_content&view=article&id=46');
	exit;
}
if(!$templateparams->get('html5', 0)): ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php else: ?>
	<?php echo '<!DOCTYPE html>'; ?>
<?php endif; ?>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="shortcut icon" href="favicon.ico">
        <script src="/templates/remontica/bundle.js"></script>
    </head>
    <body>
    <!-- main container start -->
    <div class="container" style="overflow-x: hidden;">
        <!-- top row start-->
        <div class="d-none d-lg-flex align-items-center top">
            <div class="col-4 adress-h pl-4">Республика Казахстан, г. Петропавловск, ул. Букетова, 57</div>
            <div class="col-4 work-h">Режим работы: ежедневно 10:00  - 20:00 без перерыва</div>
            <div class="col-2 p-0 text-right"><a href="https://wa.me/77471518444"><img src="/templates/remontica/images/icons/whatsapp.png" style="padding-bottom: 3px;" /></a><span class="whatsapp p-2"><a href="https://wa.me/77471518444">+7 (747) 151-84-44</a><span> </div>
            <div class="col-2 langs text-center p-0"><jdoc:include type="modules" name="language" /></div>
        </div>
        <!-- top row end-->
        <!-- main menu start-->
        <div class="row align-items-center rowMenu">
            <div class="col-6 col-md-4 col-lg-3 logo">
                <a href="<?php echo JURI::base(); ?>"><img id="logoImg" src="/templates/remontica/images/logo-bl.png" width="209px" class="img-fluid" alt="" srcset=""></a>
            </div>
            <div class="col-3 col-md-4 d-sm-block langs d-lg-none">
                <jdoc:include type="modules" name="language" />
            </div>
            <div class="col-3 col-md-4 col-md-4 d-sm-block d-sm-flex d-lg-none justify-content-end">
                <nav class="navbar py-0 px-3 navbar-expand-lg navbar-light">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                </nav>
            </div>
            <div class="col-lg-7 p-0 mainmenu">
				
            </div>
            <div class="col-lg-2 text-center d-none d-lg-block">
                <button  type="button" class="orderStatusButton" data-toggle="modal" data-target=".bd-example-modal-lg">Статус заказа</button>
            </div>
        </div>
        <!-- main menu end-->
        <!-- content start-->
		<div class="row" id="mainlayout">    
            <jdoc:include type="component" />
        </div>
        <!-- content end -->
        <div class="row" id="footer">
            <div class="col-lg-3 p-lg-4 pt-lg-5 footer-phone ">
                <img src="/templates/remontica/images/footer-logo.png" class="img-fluid pt-1" alt="" srcset="">
                <p class="phone-caption m-0 ">Телефон</p>
                <p class="footer-phone m-0 p-0">+7 (747) 151-84-44</p>
                <p class="footer-email m-0 p-0">remontika@gmail.com</p>
            </div>
            <div class="col-lg-3 p-lg-4 pt-lg-5 footer-adress">
                <p>Адрес офиса:</p>
                <p class="country m-0 p-0">Республика Казахстан, </p>
                <p class="city m-0 p-0">г.Петропавловск, ул Букетова 57</p>
                <p></p>
                <p>Режим работы: </p>
                <p class="shedule m-0 p-0">10:00 - 20:00 ежедневно, без перерыва</p>
                <jdoc:include type="modules" name="footercontacts" />
            </div>
            <div class="col-lg-3 p-0 p-lg-4 pt-lg-5 footermenu">
                <jdoc:include type="modules" name="footermenu" />
            </div>
            <div class="col-lg-3 p-0 p-lg-4 pt-lg-5 text-center footermenu2" >
                <jdoc:include type="modules" name="footermenu2" />
            </div>
        </div>
        <div class="row p-3 pt-4" id="bottom">
            <div class="col-lg-4 order-0 ">
                <jdoc:include type="modules" name="copyrights" />
            </div>
            <div class="col-lg-4 text-center order-2 order-lg-1">
                <jdoc:include type="modules" name="sociallinks" />
            </div>
            <div class="col-lg-4 float-lg-right order-1 order-lg-2">
                <jdoc:include type="modules" name="studio" />
            </div>
        </div>
    </div>
    <!-- main container end -->
        <!-- Back to button start -->
        <div id='toTop' class="text-center">
            <img src="/templates/remontica/images/icons/arrow-up.png" width="30px"/> 
        </div>
        <!-- Back to button end -->
        <!-- Check order modal start -->
	</body>

</html>
