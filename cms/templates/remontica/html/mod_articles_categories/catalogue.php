<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_categories
 *
 * @copyright   Copyright (C) 2005 - 2020 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
// Get a db connection.
$db = JFactory::getDbo();
// Create a new query object.
$query = $db->getQuery(true);
$catid = "";
$input = JFactory::getApplication()->input;
$option = $input->getCmd('option', '');
if (JRequest::getVar('option')=='com_content'):
    if (JRequest::getVar('view')=='article') {
        $query1 = $db->getQuery(true);
        $query1->select($db->quoteName('catid'));
        $query1->from($db->quoteName('#__content'));
        $query1->where($db->quoteName('id') . ' = '. $db->quote(JRequest::getInt('id')));
        
        $db->setQuery($query1);
        $catid = $db->loadResult();
        $itemid = JRequest::getInt('id');
    }
    elseif(JRequest::getVar('view')=='category'){
        $catid = JRequest::getInt('id');
    }
endif;
$i=0;
?>
<ul class="catalogue-list mb-3 mb-lg-4" id="catalogueList">
<?php foreach ($list as $item) : ?>
    <?php 		
        $query->select($db->quoteName(array('id', 'title')));
		$query->from($db->quoteName('#__content'));
		$query->where($db->quoteName('catid') . ' = ' . $db->quote($item->id));
        $query->order('ordering ASC');
        $db->setQuery($query);
		$result = $db->loadObjectList(); 
        $query->clear(); ?>
    <li class="cats <?php if(intval($item->id)!==intval($catid)){echo "collapsed";} ?>" data-toggle="collapse" data-target="#collapse<?php echo $item->id; ?>" aria-expanded="false" aria-controls="#collapse<?php echo $item->id; ?>" ><span class="cat<?php if(intval($item->id)==intval($catid)){echo "show";}?>"><?php echo $item->title;?></span>
    <?php if($result){ ?>
        <ul class="items <?php if(intval($item->id)==intval($catid)){echo "collapse show";} else{ echo "collapse";}?>" id="collapse<?php echo $item->id; ?>" aria-labelledby="#collapse<?php echo $item->id; ?>" data-parent="#catalogueList">
            <?php foreach($result as $subitem):  ?>
                <li class="item"><a class="item-link <?php if($itemid && $itemid==$subitem->id){echo "active";} ?> no-collapsable" href="<?php echo JRoute::_('index.php?option=com_content&view=article&catid='.$item->id.'&id='.$subitem->id); ?>"><?php echo $subitem->title; ?></a></li>
            <?php endforeach; ?>
        </ul>
        <?php } ?>
    </li>
    <?php $i++; ?>
<?php endforeach; ?>
</ul>
