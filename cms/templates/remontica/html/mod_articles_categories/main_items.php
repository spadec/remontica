<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_categories
 *
 * @copyright   Copyright (C) 2005 - 2020 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$input  = JFactory::getApplication()->input;
$option = $input->getCmd('option');
$view   = $input->getCmd('view');
$id     = $input->getInt('id');
// Get a db connection.
$db = JFactory::getDbo();
// Create a new query object.
$query = $db->getQuery(true);
$i=1;

?>
<div class="row mt-lg-3 ">
<?php foreach ($list as $item) : ?>
	<?php 
		if(($i % 2) !==0){
			$margin_right = "block-right";
		}
		else{
			$margin_right = "";
		}
		?>
	<div class="col-lg-5 p-0 mb-3 blocks <?php echo $margin_right; ?>">
<?php 
		$query->select($db->quoteName(array('id', 'title','images')));
		$query->from($db->quoteName('#__content'));
		$query->where($db->quoteName('catid') . ' = ' . $db->quote($item->id));
		$query->order('ordering ASC');
		$db->setQuery($query);
		$result = $db->loadObjectList(); 
		$query->clear();
        $images = json_decode($item->params);
		$image = $images->image;
?>
		<div class="block-img" style="background-image: url(<?php  echo $image; ?>); background-repeat: no-repeat; min-height: 270px;">
			<h3 class="block-title"><?php echo $item->title; ?></h3>
			<span class="block-separator"></span>
			<div class="row">
				<ul class="articles-list">
					<?php $j=1; ?>
					<?php foreach($result as $art) : ?>
					<?php if($j<=12): ?>
					<li class="article-title">
						<a class="article-link" href="<?php echo JRoute::_('index.php?option=com_content&view=article&catid='.$item->id.'&id='.$art->id); ?>"><?php echo $art->title; ?></a>
					</li>
					<?php endif; ?>
					<?php $j++;endforeach; ?>
				</ul>
			</div>
			<?php if($j>=12):?>
				<div class="allModels-title pl-4 pb-3">
					<a class="all-models" href="<?php echo JRoute::_(ContentHelperRoute::getCategoryRoute($item->id)); ?>">Все модели</a>
				</div>
			<?php endif; ?>
		</div>	
	</div>
<?php $i++; endforeach; ?>
</div>