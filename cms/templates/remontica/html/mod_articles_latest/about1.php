<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_latest
 *
 * @copyright   Copyright (C) 2005 - 2020 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<div class="row mt-5" id="about">
<div class="col-lg-6">
    <h2 class="about-title py-4"><?php echo $list[4]->title; ?></h2>
    <div class="about-text">
        <?php 
            echo trim($list[4]->introtext);
            $url =  JRoute::_(ContentHelperRoute::getArticleRoute('15', 
            '2', 
            'ru'))
        ?>
    </div>
    <a href="<?php echo $url; ?>"><button class="btn-about">Подробнее о сервисе</button></a>
</div>

