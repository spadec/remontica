<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_latest
 *
 * @copyright   Copyright (C) 2005 - 2020 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
$i = 0;
?>
<div class="row d-none d-md-block" id="slider">
    <div class="col-12 p-0 m-0 slider">
<?php foreach ($list as $item) : ?>
<?php	$images = json_decode($item->images);
	$image = $images->image_intro; 
	$urls = json_decode($item->urls);	?>
	<div class="position-relative">
		<img src="<?php echo $image; ?>" class="img-fluid" alt="">
		<div class=" col-12 slider-caption">
			<div class="col-lg-7 slide-caption">
				<h1><?php echo $item->title; ?></h1>
					<p><?php echo $images->image_intro_alt; ?></p>
                <?php if($images->image_intro_caption): ?>
                <div class="slide-button">
					<button class="btn button <?php echo $urls->urlatext; ?>"><?php echo $images->image_intro_caption; ?>  <span style="font-size: 22px;padding:5px 0 0 10px">→</span></button>
				</div>
                <?php endif; ?>
			</div>
		</div>
	</div>
<?php endforeach; ?>
    </div>
</div>