<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_latest
 *
 * @copyright   Copyright (C) 2005 - 2020 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<div class="col-lg-6 pt-4">
<?php foreach ($list as $item) : ?>
<?php 
    $images = json_decode($item->images);
    $urls = json_decode($item->urls);
    $image = $images->image_intro;
    $url = $urls->urla; 
    $url_text = $urls->urlatext; 
    if($item->id !== "19"): ?>
    <div class="row justify-content-end py-1">
        <div class="col-3 col-md-2 align-self-center text-center">
            <img src="<?php echo $image; ?>" alt="" srcset="">
        </div>
        <div class="col-9 col-md-9 about-list">
            <h4 class=""><?php echo $item->title; ?></h4>
            <p><?php echo $item->introtext; ?></p>
        </div>
    </div>
    <?php endif; ?>
<?php endforeach; ?>
</div>


</div>