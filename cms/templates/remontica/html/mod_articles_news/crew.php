<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_latest
 *
 * @copyright   Copyright (C) 2005 - 2020 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<div class="row mt-5 " id="crew">
    <h2 class="crew-title my-2 mb-3"><?php echo $module->title; ?></h2>
    <p class="crew-info"><?php echo $moduleclass_sfx; ?></p>
    <div class="col-12 p-0 m-0 d-flex justify-content-md-center crew-carousel">
        <?php foreach ($list as $item) : ?>
        <?php
            $images = json_decode($item->images);
            $urls = json_decode($item->urls);
            $image = $images->image_intro;
            $url = $urls->urla; 
            $url_text = $urls->urlatext; 
        ?>
        <div class="p-0" id="crew-block">
            <div class="crew-img"><a href="<?php echo $item->link; ?>"><img class="img-fluid" src="<?php echo $image; ?>" /></a></div>
            <a class="crew-about text-center" href="<?php echo $item->link; ?>">
                <p class="crew-name"><?php echo $item->title; ?></p>
                <p class="crew-caption"><?php echo $images->image_intro_caption; ?></p>
                <p class="crew-alt"><?php echo $images->image_intro_alt; ?></p>
            </a>
        </div>
        <?php endforeach; ?>
    </div>
</div>