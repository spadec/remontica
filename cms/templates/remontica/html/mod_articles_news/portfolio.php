<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_latest
 *
 * @copyright   Copyright (C) 2005 - 2020 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<div class="row mt-5" id="portfolio">
    <div class="col-12 d-md-flex px-lg-0 mb-4 ptitle">
        <div class="col-12 col-md-8 px-0"><h2><?php echo $module->title; ?></h2></div>
        <div class="col-12 col-md-4 px-0 allworks"><a href="#"><button class="btn-portfolio">Все работы</button></a></div>
    </div>
    <div class="col-12 d-lg-flex px-lg-0 pbody">
        <?php foreach ($list as $item) : ?>
            <?php 
                $images = json_decode($item->images);
                $urls = json_decode($item->urls);
                $image_intro = $images->image_intro;
                $fullimg = $images->image_fulltext;
                $url = $urls->urla; 
                $url_text = $urls->urlatext; 
            ?>
            <div class="row">
            <div class="col-12 col-lg-8 p-0 pr-lg-1 portfolio-img order-1 order-lg-0">
                <img src="<?php echo $fullimg; ?>" class="img-fluid" alt="<?php echo $item->image_intro_caption; ?>" srcset="">
            </div>
            <div class="col-12 col-lg-4 p-4 portfolio-desc order-0 order-lg-1">
                <div class="row align-items-center text-left px-lg-3">
                    <div class="col-5 col-md-3 col-lg-5"><img src="<?php echo  $image_intro; ?>" alt="" srcset=""></div>
                    <div class="col-7 col-md-5 col-lg-7 pl-0">
                        <h4 class="portfolio-title m-0"><?php echo $item->title; ?></h4>
                        <p class="portfolio-alt m-0"><?php echo $images->image_intro_alt; ?></p>
                        <p class="portfolio-service m-0"><?php echo $images->image_intro_caption; ?></p>
                    </div>
                </div>
                <div class="row mt-3 px-lg-4">
                    <div class="col-12 text-justify">
                        <?php echo $item->introtext; ?>
                    </div>
                </div>
            </div>  
            </div>
        <?php endforeach; ?>
    </div>
</div>