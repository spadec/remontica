<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_banners
 *
 * @copyright   Copyright (C) 2005 - 2020 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JLoader::register('BannerHelper', JPATH_ROOT . '/components/com_banners/helpers/banner.php');
?>
<div class="row d-none d-md-block" id="mainPageBanner">
    <?php foreach ($list as $item) : ?>
        <?php $link = $item->clickurl; ?>
        <?php echo str_replace(array('{CLICKURL}', '{NAME}'), array($link, $item->name), $item->custombannercode); ?>

        <!--<a class="bannerimg" style="background-image: url(<?php echo $item->params->get('imageurl'); ?>);" href="<?php echo $link; ?>"></a> -->
        <div class=""><a href="<?php echo $link; ?>"><img src="<?php echo $item->params->get('imageurl'); ?>" class="img-fluid" alt="" srcset=""></a></div>
        <?php endforeach; ?>
</div>