<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_banners
 *
 * @copyright   Copyright (C) 2005 - 2020 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JLoader::register('BannerHelper', JPATH_ROOT . '/components/com_banners/helpers/banner.php');
?>
<div class="row d-sm-block d-md-none" id="mainPageMobileBanner">
    <?php foreach ($list as $item) : ?>
        <?php $link = $item->clickurl; ?>
        <div style="background-image: url(/templates/remontica/images/bluebanner.jpg);" class="mobilebanner">
            <a href="<?php echo $link; ?>">
                <div class="bannerimg" style="background-image: url(<?php echo $item->params->get('imageurl'); ?>);" ></div>
                <div class="banner-caption">
                    <div class="banner-title"><?php echo $item->name; ?></div>
                    <div class="banner-desc"><?php echo $item->description; ?></div>
                    <div class="banner-alt"><?php echo $item->params->get('alt'); ?></div>
                </div>
            </a>
        </div>
        
        <?php endforeach; ?>
</div>