<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_custom
 *
 * @copyright   Copyright (C) 2005 - 2020 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<div class="row" id="cinfo">
    <div class="col-lg-12"><h2 class="title px-2">Как с нами связаться:</h2></div>
    <div class="col-lg-12 d-md-flex py-2 py-md-5 contacts-info">
        <div class="col-md-4 px-2 px-md-3 pl-lg-3 separator">
            <p><img src="/images/icons/Mobile@2x.png" alt="" /></p>
            <strong><span class="phones-title">Телефоны</span></strong><br>
            <span class="phones">+7(747) 151-84-44</span><br>
            <div class="cseparator"></div>
        </div>
        <div class="col-md-4 px-2 px-md-3 pl-2 pl-lg-5 separator">
           <p> <img src="/images/icons/LocationPin2x.png" alt="" /></p>
            <strong><span class="phones-title">Адрес сервисного центра:</span></strong><br>
            <span class="phones">Республика Казахстан,<br>
            г.Петропавловск, ул. Буктова, 57 </span><br>
        </div>
        <div class="col-md-4 px-2 px-md-3 pl-2 pl-lg-5">
        <p> <img src="/images/icons/Mails@2x.png" alt="" /></p>
            <strong><span class="phones-title">Электронный адрес:</span></strong><br>
            <span class="phones"><a href="mailto:info@remontika.com">info@remontika.com</a></span><br>
        </div>
    </div>
</div>
<div class="row p-0 mb-3 contact-form" >
    <div class="col-lg-6 p-5" id="props">
        <h2>Реквизиты</h2>
        <div class="props-info">
        ТОО "Quadriga" <br>
        БИН 170940003901<br>
        150000, Республика Казахстан, Северо-Казахстанская область, г. Петропавловск, ул Букетова, 57<br>
        т. 8(747)151-84-44<br>
        contact@remontika.com<br>
        ИИК (IBAN счет) KZ0396508F0007220852<br>
        БИК IRTYKZKA<br>
        Филиал АО "ForteBank" в г. Петропавловск<br>
        Директор Самохин Денис Геннадьевич<br>

        </div>
    </div>
    <div class="col-lg-6 mr-0 p-5" id="cform">
        <h2 class="mb-4">Связаться с нами</h2>
        <form action="" method="post">
            <input type="text" required name="cname" id="cname" placeholder="ФИО"/><br>
            <input type="text" required name="cphone" id="cphone" /><br>
            <input type="email" required name="cemail" id="cemail" placeholder="email" /><br>
            <textarea name="message" id="message" cols="50" rows="5" placeholder="Сообщение"></textarea><br>
            <br><button class="btn-contacts" type="submit">Отправить</button>
        </form>
    </div>
</div>
<?php
if(isset($_POST["cemail"])){
    $mailer = JFactory::getMailer();
    $email = $_POST["cemail"];
    $name = $_POST['cname'];
    $phone = $_POST['cphone'];
    $message = "Имя: ".$name."<br />";
    $message .= "Email: ".$email."<br />";
    $message .= "Телефон: ".$phone."<br />";
        $mailer->setSubject('Сообщение со страницы контакты сайта Ремонтика');
        $mailer->IsHTML( true );
        $mailer->setSender( array( 'paul.starkov@gmail.com', 'http://remontica.my/') );
        $mailer->addRecipient( 'paul.starkov@gmail.com' );
        $mailer->setBody($message);
        $status = $mailer->send();
            if($status==1){ ?>
                <div class="modal" id="myModal" tabindex="-1" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="p-3 text-center">
                                <h3 class="">Сообщение успешно отправлено!</h5>
                            </div>
                            <div class="px-3 text-center">
                                <p>Спасибо за обращение. Наши менеджеры свяжутся с Вами в ближайшее время </p>
                            </div>
                        </div>
                    </div>
                </div>
        
        <?php  //$app->enqueueMessage(JText::_('Ваше сообщение успешно отправленно'), 'success');
        } 
}