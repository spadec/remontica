<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_custom
 *
 * @copyright   Copyright (C) 2005 - 2020 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<div class="<?php echo $moduleclass_sfx; ?>" >
    <?php echo $module->content; ?>
    © 2010-<?php echo Date('Y'); ?>. Ремонтика. Все права защищены.
</div>
