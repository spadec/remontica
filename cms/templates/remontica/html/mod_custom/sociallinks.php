<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_custom
 *
 * @copyright   Copyright (C) 2005 - 2020 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<div class="sociallinks">
    <div class="col-12 d-flex justify-content-center">
        <div class="twitter m-1" ><a href="#"></a></div>
        <div class="facebook m-1" ><a href="#"></a></div>
        <div class="vk m-1" ><a href="#"></a></div>
        <div class="insta m-1" ><a href="#"></a></div>
    </div>
</div>