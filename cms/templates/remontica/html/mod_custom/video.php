<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_custom
 *
 * @copyright   Copyright (C) 2005 - 2020 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<div class="row video-wrapper" data-toggle="modal" data-src="" data-target="#vModal" id="video" style="background-image:url(<?php echo $params->get('backgroundimage'); ?>)">
	<div class="col-lg-12 text-center my-auto">
		<p><span id="playbackButton" data-toggle="modal" data-src="" data-target="#vModal" ><img src="/images/icons/Play@2x.png" alt=""></span></p>
		<button class="btn button video-btn"  data-toggle="modal" data-src="" data-target="#vModal" id="videoButton">ВИДЕОРОЛИК</button>
		<h2><?php echo $module->title; ?></h2>
		<p class="video-description"><?php echo strip_tags($module->content); ?>></p>
	</div>
	<!-- Modal -->
<div class="modal fade" id="vModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-body p-0">     
		<div class="embed-responsive embed-responsive-16by9">
			<iframe class="embed-responsive-item" src="<?php echo $moduleclass_sfx; ?>" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
		</div>
      </div>
    </div>
  </div>
</div> 
</div>