<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_menu
 *
 * @copyright   Copyright (C) 2005 - 2020 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$id = '';

if ($tagId = $params->get('tag_id', ''))
{
	$id = ' id="' . $tagId . '"';
}
// The menu class is deprecated. Use nav instead
?>
<div id="mainmenu">
	<div class="col-lg-12 p-0">
		<nav class="navbar p-0 navbar-expand-lg navbar-light">
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<?php foreach ($list as $i => &$item)
				{
					$class = 'nav-item';
					$attr = 'class="nav-link" href="'.$item->flink.'"';

					if ($item->parent)
					{
						$class .= " dropdown";
						$attr = 'class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"';
					}
					if (in_array($item->id, $path))
					{
						$class .= ' active';
					}
					elseif ($item->type === 'alias')
					{
						$aliasToId = $item->params->get('aliasoptions');
				
						if (count($path) > 0 && $aliasToId == $path[count($path) - 1])
						{
							$class .= ' active';
						}
						elseif (in_array($aliasToId, $path))
						{
							$class .= ' alias-parent-active';
						}
					}
					echo '<li class="'.$class.'"><a '.$attr.' >'.$item->title.'</a>';
					// The next item is deeper.
					if ($item->deeper)
					{
						echo '<div class="dropdown-menu" aria-labelledby="navbarDropdown">';
						echo '<ul>';
					}
					// The next item is shallower.
					elseif ($item->shallower)
					{
						echo '</li>';
						echo str_repeat('</ul></li>', $item->level_diff);
					}
					// The next item is on the same level.
					else
					{
						echo '</li>';
					}
				}
				?>
			</ul>
			<div class="d-lg-none d-md-block">
				<p class="mobile-adress"> Республика Казахстан, г. Петропавловск, <br>
				ул. Букетова, 57</p>
				<p class="mobile-phone">+7 (747) 151-84-44 </p>
				<div class="row align-items-center">
					<p style="margin-right:5px"><button class="mobile-status orderStatusButton" data-toggle="modal" data-target=".bd-example-modal-lg">Статус заказа</button></p>
					<p><a href="https://wa.me/77471518444" class="mobile-whatssapp"><img style="margin-right:5px" width="16px" src="templates/remontica/images/icons/Whats_app.png" alt="" srcset="" />Whatsapp</a></p>
				</div>
			</div>
		</div>
		</nav>
	</div>
</div>
