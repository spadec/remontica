<?php defined( '_JEXEC' ) or die( 'Restricted access' );?>
<?php
	$this->setGenerator('');
	$this->_scripts = array();
	unset($this->_script['text/javascript']);
	$lang = JFactory::getLanguage();
	$lang_tag = $lang->getTag();
	$languages	= JLanguageHelper::getLanguages();
    $lang_index = 0;
    if($lang_tag=="ru-RU"){
        $title = "";
        $subtitle = "";
    }
    if($lang_tag=="kk-KZ"){
        $title = "";
        $subtitle = "";
    }
    $catid = "";
    $input = JFactory::getApplication()->input;
	$option = $input->getCmd('option', '');
	if (JRequest::getVar('option')=='com_content'):
		if (JRequest::getVar('view')=='article') {
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select($db->quoteName('catid'));
			$query->from($db->quoteName('#__content'));
			$query->where($db->quoteName('id') . ' = '. $db->quote(JRequest::getInt('id')));
			$db->setQuery($query);
			$catid = $db->loadResult();
		}
		elseif(JRequest::getVar('view')=='category'){
			$catid = JRequest::getInt('id');
		}
    endif;
    $db = JFactory::getDbo();
    $query = $db->getQuery(true);
    $query->select($db->quoteName('parent_id'));
    $query->from($db->quoteName('#__categories'));
    $query->where($db->quoteName('id') . ' = '. $db->quote($catid));
    $db->setQuery($query);
    $parentcatid = $db->loadResult();
?>
<?php

?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="shortcut icon" href="favicon.ico">
        <jdoc:include type="head" />
        <script src="/templates/remontica/bundle.js"></script>
    </head>
    <body>
    <!-- main container start -->
    <div class="container" style="overflow-x: hidden;">
        <!-- top row start-->
        <div class="d-none d-lg-flex align-items-center top">
            <div class="col-4 adress-h pl-2">Республика Казахстан, г. Петропавловск, ул. Букетова, 57</div>
            <div class="col-4 work-h">Режим работы: ежедневно 10:00  - 20:00 без перерыва</div>
            <div class="col-2 p-0 text-right"><a href="https://wa.me/77471518444"><img src="/templates/remontica/images/icons/whatsapp.png" style="padding-bottom: 3px;" /></a><span class="whatsapp p-2"><a href="https://wa.me/77471518444">+7 (747) 151-84-44</a><span> </div>
            <div class="col-2 langs text-center p-0"><jdoc:include type="modules" name="language" /></div>
        </div>
        <!-- top row end-->
        <!-- main menu start-->
        <div class="row align-items-center rowMenu">
            <div class="col-6 col-md-4 col-lg-3 logo">
                <a href="<?php echo JURI::base(); ?>"><img id="logoImg" src="/templates/remontica/images/logo-bl.png" width="209px" class="img-fluid" alt="" srcset=""></a>
            </div>
            <div class="col-3 col-md-4 d-sm-block langs d-lg-none">
                <jdoc:include type="modules" name="language" />
            </div>
            <div class="col-3 col-md-4 col-md-4 d-sm-block d-sm-flex d-lg-none justify-content-end">
                <nav class="navbar py-0 px-3 navbar-expand-lg navbar-light">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                </nav>
            </div>
            <div class="col-lg-7 p-0 mainmenu">
                <jdoc:include type="modules" name="mainmenu" />
            </div>
            <div class="col-lg-2 text-center d-none d-lg-block">
                <button  type="button" class="orderStatusButton" data-toggle="modal" data-target=".bd-example-modal-lg">Статус заказа</button>
            </div>
        </div>
        <!-- main menu end-->
        <!-- content start-->
            <?php if(JURI::current() == JURI::base() || $parentcatid !=="10" ){ ?>
                <!--  slider start-->
                <jdoc:include type="modules" name="slider" />
                <!--  slider end -->
                <div class="row" id="mainlayout">    
                    <jdoc:include type="component" />
                </div>
            <?php } ?>
            <?php if($parentcatid && $parentcatid=="10") {  ?>
                <div class="row" id="cataloglayout">
                    <div class="col-lg-3 pl-lg-0 mt-3 mt-lg-4">
                        <jdoc:include type="modules" name="leftside" />
                    </div>
                    <div class="col-lg-9 mt-0">
                        <jdoc:include type="modules" name="slider" />
                        <jdoc:include type="component" />
                    </div>
                </div>
            <?php } ?>
        <jdoc:include type="modules" name="catalogue" />
        <jdoc:include type="modules" name="about" />
        <jdoc:include type="modules" name="about2" />
        <jdoc:include type="modules" name="bluebanner" />
        <jdoc:include type="modules" name="video" />
        <jdoc:include type="modules" name="crew" />
        <jdoc:include type="modules" name="portfolio" />
        <!-- content end -->
        <div class="row" id="footer">
            <div class="col-lg-3 p-lg-4 pt-lg-5 footer-phone ">
                <img src="/templates/remontica/images/footer-logo.png" class="img-fluid pt-1" alt="" srcset="">
                <p class="phone-caption m-0 ">Телефон</p>
                <p class="footer-phone m-0 p-0">+7 (747) 151-84-44</p>
                <p class="footer-email m-0 p-0">info@remontika.com</p>
            </div>
            <div class="col-lg-3 p-lg-4 pt-lg-5 footer-adress">
                <p>Адрес сервисного центра:</p>
                <p class="country m-0 p-0">Республика Казахстан, </p>
                <p class="city m-0 p-0">г.Петропавловск, ул Букетова 57</p>
                <p></p>
                <p>Режим работы: </p>
                <p class="shedule m-0 p-0">10:00 - 20:00 ежедневно, без перерыва</p>
                <jdoc:include type="modules" name="footercontacts" />
            </div>
            <div class="col-lg-3 p-0 p-lg-4 pt-lg-5 footermenu">
                <jdoc:include type="modules" name="footermenu" />
            </div>
            <div class="col-lg-3 p-0 p-lg-4 pt-lg-5 text-center footermenu2" >
                <jdoc:include type="modules" name="footermenu2" />
            </div>
        </div>
        <div class="row p-3 pt-4" id="bottom">
            <div class="col-lg-4 order-0 ">
                <jdoc:include type="modules" name="copyrights" />
            </div>
            <div class="col-lg-4 text-center order-2 order-lg-1">
                <jdoc:include type="modules" name="sociallinks" />
            </div>
            <div class="col-lg-4 order-1 order-lg-2">
                <jdoc:include type="modules" name="studio" />
            </div>
        </div>
    </div>
    <!-- main container end -->
        <!-- Back to button start -->
        <div id='toTop' class="text-center">
            <img src="/templates/remontica/images/icons/arrow-up.png" width="30px"/> 
        </div>
        <!-- Back to button end -->
        <!-- Check order modal start -->
        <div class="modal fade bd-example-modal-lg" tabindex="-1" id="orderStatus" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body pt-0 pl-5 pr-5 pb-4">
                        <h2 class="modal-title" id="myLargeModalLabel">Проверить статус ремонта</h2>
                        <form action="" name="orderStatus" method="post">
                            <div class="m-0 mt-4 d-md-flex">
                                <input required pattern="\d+" type="number" name="kvitok" id="number" placeholder=" *Номер заказа"/>
                                <input type="text" class="datepicker" name="date" id="date" placeholder="Дата приема" />
                            </div>
                            <div class="orderStatusCaption">Введите номер Заказа на услуги и дату приема:</div>
                            <button type="button" id="orderStatusButton" class="btn-orderStatus mt-3" >Проверить</button>
                            <div class="loader text-center"><img src="templates/remontica/loader.gif" alt="" srcset=""></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
	</body>
</html>
