var FtpDeploy = require("ftp-deploy");
var ftpDeploy = new FtpDeploy();
var myArgs = process.argv.slice(2);
console.log('myArgs: ', myArgs);
var basedir = "/cms",
    localdir = basedir+"/templates/remontica/",
    remotedir = "/templates/remontica/";
switch (myArgs[0]) {
case '-path':
    localdir = basedir+myArgs[1],
    remotedir = myArgs[1];
    break;
case 'compliment':
    console.log(myArgs[1], 'is really cool.');
    break;
default:
    localdir = basedir+"/templates/3gp/",
    remotedir = "/templates/3gp/";
}
var config = {
    user: "remontica",
    // Password optional, prompted if none given
    password: "r3$t0T6f",
    host: "remontica.com",
    port: 21,
    localRoot: __dirname + localdir,
    remoteRoot: remotedir,
    // include: ["*", "**/*"],      // this would upload everything except dot files
    include: ["*", "**/*"],
    // e.g. exclude sourcemaps, and ALL files in node_modules (including dot files)
    exclude: ["dist/**/*.map", "node_modules/**", "node_modules/**/.*", ".git/**"],
    // delete ALL existing files at destination before uploading, if true
    deleteRemote: false,
    // Passive mode is forced (EPSV command is not sent)
    forcePasv: true
};
 
// use with promises
ftpDeploy
    .deploy(config)
    .then(res => console.log("finished:", res))
    .catch(err => console.log(err));