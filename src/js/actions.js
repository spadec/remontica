$.fn.datepicker.dates['ru'] = {
  days: ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"],
  daysShort: ["Вск", "Пнд", "Втр", "Срд", "Чтв", "Птн", "Суб"],
  daysMin: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
  months: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
  monthsShort: ["Янв", "Фев", "Мар", "Апр", "Май", "Июн", "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек"],
  today: "Сегодня",
  clear: "Очистить",
  format: "dd.mm.yyyy",
  weekStart: 1,
  monthsTitle: 'Месяцы'
};
$.ajaxSetup({ // параметры ajax-запроса по умолчанию 
  type: 'POST',
  dataType: 'json',
    beforeSend: function() {
      $('.loader').show();
    },
    complete: function() {
      $('.loader').hide();
    },
    error: function(jqXHR, textStatus, errorThrown) {
      console.log('Ошибка: ' + textStatus + ' | ' + errorThrown);
      $('.loader').hide();
    }
});

/**
 * Действие при загрузки страницы
 */
$(document).ready(function(){
  function renderAlert(mes, type){
    $(".alert").remove();
    var html = '<div class="alert alert-'+type+'" role="alert">'+mes+'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
    return html;
  }
  $("#orderStatusButton").click(function(){
    if($("#number").val()){
      $.ajax({
        url:'/templates/remontica/status.php',
        data: {kvitok:$("#number").val(),date:$("#date").val()},
      }).done(function(res){
        $(renderAlert(res.mes,res.type)).insertAfter(".orderStatusCaption");
      }).fail(function(err){
        $(renderAlert(err,'danger')).insertAfter(".orderStatusCaption");
      });
    }
    else {
      $(renderAlert('Введите номер заказа','danger')).insertAfter(".orderStatusCaption");
    }
  });
  /**
   * Кнопка вернутся наверх
   */
  $(window).scroll(function() {
    if ($(this).scrollTop()) {
        $('#toTop').fadeIn();
    } else {
        $('#toTop').fadeOut();
    }
  });
  /**
   * Кнопка поднятся наверх
   */
  $("#toTop").click(function() {
    $("html, body").animate({scrollTop: 0}, 1000);
  });
  /**
   * слайдер на главной
   */
  $('.slider').slick({
    dots: true,
    autoplay: true,
    autoplaySpeed: 3000,
    arrows:false,
    responsive: [
      {
        breakpoint: 760,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          dots: false,
          arrows:false
        }
      }
    ]
  });
  /**
   * кнопки управления в карусели "Наша команда"
   */
  $(document).on("mouseenter", ".crew-img", function(e) {
    $(this).next().attr("style","background-color:#d55110");
   // $('.slick-next').show();
    //$('.slick-prev').show();
  });
  $(document).on("mouseleave", ".crew-img", function(e) {
    $(this).next().attr("style","background-color:#000000");
    //$('.slick-next').hide();
    //$('.slick-prev').hide();
  });
  $(document).on("mouseenter", ".crew-about", function(e) {
    $(this).attr("style","background-color:#d55110");
    //$('.slick-next').show();
    //$('.slick-prev').show();
  });
  $(document).on("mouseleave", ".crew-about", function(e) {
    $(this).attr("style","background-color:#000000");
    //$('.slick-next').hide();
    //$('.slick-prev').hide();
  });
  /**
   * Карусель "Наша команда"
   */
  $('.crew-carousel').slick({
    dots: false,
    infinite: true,
    speed: 300,
    slidesToShow: 3,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          infinite: true,
          dots: false,
          arrows:false
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          dots: false,
          arrows:false
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          dots: false,
          arrows:true
        }
      }
      // You can unslick at a given breakpoint now by adding:
      // settings: "unslick"
      // instead of a settings object
    ]
  });
  //$('.slick-next').hide();
  //$('.slick-prev').hide();
  /**
   * Выбор даты в модуле "Статус заказа"
   */
  $('.datepicker').datepicker({
    format: 'dd-mm-yyyy',
    language: 'ru',
    todayHighlight: true,
  });
  /**
   * Убираем коллапс при переходе по ссылке в категориях каталога
   */
  $('.no-collapsable').on('click', function (e) {
    e.stopPropagation();
  });
  /**
   * "Шторка" меню в мобильной версии
   */
  $('#navbarSupportedContent').on('show.bs.collapse', function () {
    $(".navbar-toggler-icon").attr("id","closeIcon");
    $(".rowMenu").addClass("rowMenuBlack");
    $(".langs").addClass("langs-black");
    $("#logoImg").attr("src","/templates/remontica/images/logo-bl-wh.png");
  });
  $('#navbarSupportedContent').on('hide.bs.collapse', function () {
    $(".navbar-toggler-icon").attr("id","");
    $(".rowMenu").removeClass("rowMenuBlack");
    $(".langs").removeClass("langs-black");
    $("#logoImg").attr("src","/templates/remontica/images/logo-bl.png");
  });
  /**
   * показывает модальное окно когда отправленно сообщение
   */
  if($('#myModal').length > 0){
    $('#myModal').modal('show');
  }
  if($('#orderModal').length > 0){
    $('#orderModal').modal('show');
  }
  $("#cphone").mask('+7(000) 000-00-00', {placeholder: "Телефон"});
});
